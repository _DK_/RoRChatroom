Rails.application.routes.draw do
  resources :emojis
  devise_for :users
  resources :users
 	resources :messages

	# Home page as root
  root 'home#home'

  # Contact page
  get '/contact' => 'contact#contact'
  post 'request_contact' => 'contact#request_contact'

end
