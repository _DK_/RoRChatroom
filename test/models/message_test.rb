require 'test_helper'

class MessageTest < ActiveSupport::TestCase

  setup do
  	@user = users(:one)
  end

  test 'should not save invalid message' do
  	message = Message.new

  	message.save
  	refute message.valid?
  end

  test 'should save valid message' do
  	message = Message.new

  	message.user = @user
  	message.message = "Message!"
  	
  	message.save
  	assert message.valid?
  end

end
