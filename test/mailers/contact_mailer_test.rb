require 'test_helper'

class ContactMailerTest < ActionMailer::TestCase
  test "should return contact email" do 
  	mail = ContactMailer.contact_email("Dave", "dave@dave.dave", "dave")
  	assert_equal ['to@example.com'], mail.to
  	assert_equal ['from@example.com'], mail.from
  end
end
