require 'test_helper'

class EmojisControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  
  setup do
    @emoji = emojis(:one)
    @user = users(:one)
    sign_in @user
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create emoji" do
    assert_difference('Emoji.count') do
      post :create, params: { emoji: { description: @emoji.description, img_url: @emoji.img_url, name: @emoji.name } }
    end

    assert_redirected_to emojis_path
  end

  test "should show emoji" do
    get :edit, params: { id: @emoji }
    assert_response :success
  end

  test "should get edit" do
    get :edit, params: { id: @emoji }
    assert_response :success
  end

  test "should update emoji" do
    patch :update, params: { id: @emoji, emoji: { description: @emoji.description, img_url: @emoji.img_url, name: @emoji.name } }
    assert_redirected_to emoji_path(assigns(:emoji))
  end

  test "should destroy emoji" do
    assert_difference('Emoji.count', -1) do
      delete :destroy, params: { id: @emoji }
    end

    assert_redirected_to emojis_path
  end
end