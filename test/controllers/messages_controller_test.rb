require 'test_helper'

class MessagesControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers
  
  setup do
    @message = messages(:one)
    @user = users(:one)
    sign_in @user
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_redirected_to messages_path
  end

  test "should create message" do
    assert_difference('Message.count') do
      post :create, params: { message: { message: @message.message } }
    end

    assert_redirected_to messages_path
  end
end
