require 'test_helper'

class HomeControllerTest < ActionController::TestCase
	include Devise::Test::ControllerHelpers
	
	setup do
		@user = users(:one)
  	sign_in @user
	end

  test "should get home" do
    get :home

    assert_response :success
    assert_template :application

    assert_select 'title', 'Chatr'
    assert_select '#title', 'Chatr'
    assert_select 'p', '© Chatr 2016'
  end

end
