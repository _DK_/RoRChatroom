require 'test_helper'

class ContactControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  test "should get contact" do
    get :contact
    
    assert_response :success
    assert_template :application

    assert_select 'title', 'Chatr'
    assert_select '#title', 'Chatr'
    assert_select 'footer p', '© Chatr 2016'

    assert_select 'h2', 'Get in touch'
    assert_select 'h5', 'Complete the form below to send us a message:'
  end

  test "should post requst contact but fail" do
  	post :request_contact

  	assert_response :redirect
  	assert_not_empty flash[:alert]
  	assert_nil flash[:notice]
  end

  test "should post request contact" do
  	post :request_contact, params: { name: "Dave", email: "Dave@dave.dave", message: "dave" }

  	assert_response :redirect
  	assert_not_empty flash[:notice]
  	assert_nil flash[:alert]
	end

end
