class ContactController < ApplicationController
  def request_contact
  	name = params[:name]
  	email = params[:email]
  	message = params[:message]

  	if name.blank? || message.blank? || email.blank?
  		flash[:alert] = I18n.t('contact.request_contact.missing_field')
  	else
      ContactMailer.contact_email(name, email, message).deliver_now
  		flash[:notice] = I18n.t('home.request_contact.email_sent')
  	end

  	redirect_to root_path
  end
end