class MessagesController < ApplicationController
  before_action :authenticate_user!

  # GET /messages
  # GET /messages.json
  def index
    @messages = Message.order('created_at ASC')
    respond_to do |format|
      format.html { render :index }
      format.json { render json: @messages }
    end
  end

  # GET /messages/new
  def new
    redirect_to messages_url
  end

  # POST /messages
  # POST /messages.json
  def create
    @message = Message.new(message_params)
    @message.user = current_user

    respond_to do |format|
      if @message.save
        format.html { redirect_to messages_url, notice: 'Message was successfully created.' }
        format.js { flash[:notice] = 'Message was successfully created.' }
        format.json { render :show, status: :created, location: @message }
      else
        format.html { render :new }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end
  
  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
      params.require(:message).permit(:message)
    end
end
