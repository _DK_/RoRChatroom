class HomeController < ApplicationController
	before_action :authenticate_user!
  def home
    @message = Message.new
  end
end
