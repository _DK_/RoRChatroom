json.extract! emoji, :id, :name, :description, :img_url, :created_at, :updated_at
json.url emoji_url(emoji, format: :json)