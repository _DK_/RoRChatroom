# Polling script to poll messages every second
window.Poll = ->
	setInterval ->
		$.get '/messages', ((response) ->
			messages = ""
			for k in response
				messages = messages + '<p><span>User ' + k["user_id"] + ': </span>' + k["message"] + '</p>'
			$('#chatbox').html(messages)
		), null, 'json'
		$.get '/emojis', ((response) ->
			for k in response
				text = $('#chatbox').html().replace(RegExp(k["name"],'g'), '<img src="' + k["img_url"] + '" title="' + k["description"] + '"/> <br />')
				$('#chatbox').html(text)
				$('#chatbox').scrollTop($('#chatbox')[0].scrollHeight)
		), null, 'json'
	, 3000

jQuery ->
	Poll() if $('#chatbox').size() > 0