class Message < ApplicationRecord
  belongs_to :user
  validates :user, :message, presence: true
end
