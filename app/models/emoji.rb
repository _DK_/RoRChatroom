class Emoji < ApplicationRecord
	validates :name, :img_url, presence: true
end
