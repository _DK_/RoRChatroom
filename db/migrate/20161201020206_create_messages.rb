class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.belongs_to :user, index: true, foreign_key: true, null: false
      t.string :message, null: false

      t.timestamps
    end
  end
end
