class CreateEmojis < ActiveRecord::Migration[5.0]
  def change
    create_table :emojis do |t|
      t.string :name, null: false, unique: true
      t.string :description
      t.string :img_url, null: false

      t.timestamps
    end
  end
end
